import { ordered,restocked } from './cakeSlice'
import { useAppDispatch, useAppSelector } from '../../app/hooks'


export function CakeView() {
   const numberOfCakes =  useAppSelector((state)=> state.cake.numOfCakes)
   const dispatch = useAppDispatch()
  return (
    <div>
    <h2>Number o cakes -  {numberOfCakes}</h2>
    <button onClick={()=>dispatch(ordered())}>Order Cake</button>
    <button  onClick={()=>dispatch(restocked(5))}>Restock Cake</button>
    </div>
  )
}


