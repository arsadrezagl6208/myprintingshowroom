import { useState } from 'react'
// import { useSelector, useDispatch } from 'react-redux'
import { ordered, restocked } from './icecreamSlice'
import { useAppDispatch, useAppSelector } from '../../app/hooks'


export function IcecreamView() {
  const [value,setValue] = useState(1);
  const numOfIcecreams =  useAppSelector((state)=> state.icecream.numOfIcecreams)
  const dispatch = useAppDispatch()
  return (
    <div>
    <h2>Number o ice creams - {numOfIcecreams} </h2>
    <button  onClick={()=>dispatch(ordered())}>Order ice creams</button>
    <input type='number' value={value} onChange={(e)=>setValue(parseInt(e.target.value))}></input>
    <button  onClick={()=>dispatch(restocked(value))}>Restock ice creams</button>
    </div>
  )
}

// export default IcecreamView
