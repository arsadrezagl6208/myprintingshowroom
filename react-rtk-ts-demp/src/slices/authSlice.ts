import { createSlice } from '@reduxjs/toolkit';

interface AuthState {
  email: string;
  password: string;
  loading: boolean;
}

const initialState: AuthState = {
  email: '',
  password: '',
  loading: false,
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setEmail: (state, action) => {
      state.email = action.payload;
    },
    setPassword: (state, action) => {
      state.password = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
  },
});