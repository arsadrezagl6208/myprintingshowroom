import React from "react";
import { Grid } from "@mui/material";
import AppFlow from "./layout/AppFlow";
import NavSider from "./layout/navSider/NavSider";
import NavHeader from "./layout/navHeader/NavHeader";
import ContainerWrapper from "../../Register-Login/ContainerWrapper";
import "./MainPage.scss";

export const Dashboard = () => {
  return (
    <section className="MainApp">
      <NavHeader />
      {/* <Content> */}
      <ContainerWrapper className="main-section">
        <Grid container spacing={2}>
          <Grid item xs={2}>
            <NavSider />
          </Grid>
          <Grid item xs={10}>
            <div style={{ paddingLeft: 24 }}>
              <AppFlow />
            </div>
          </Grid>
        </Grid>
        {/* </Content> */}
        <div className="bgapp" />
      </ContainerWrapper>
    </section>
  );
};

