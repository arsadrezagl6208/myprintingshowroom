import { memo } from "react";
import { Avatar, Card } from "antd";
import Grid from "@mui/material/Grid";
import ArrowRightAltIcon from "@mui/icons-material/ArrowRightAlt";

import "./Card.scss";

type Props = {
  contentHeading1?: String;
  contenHeading2?: string;
  value?: any;
  id?: string;
  color?: string;
};

const Cardbox = ({
  contentHeading1,
  contenHeading2,
  value,
  color,
  id = "",
}: Props) => {
  return (
    <Grid className="card-container" item xs={12} sm={12}>
      <Card
        className="card"
        style={{ backgroundColor: color, border: color }}
        id={id}
      >
        <div className="card-wrap">
          <Avatar
            className="avatar-1"
            size={60}
            shape="square"
            style={{ color: color }}
          >
            {value}
          </Avatar>
          <p>
            {contentHeading1}
            <br />
            {contenHeading2}
          </p>
        </div>
        <div className="viewlist-btn">
          <p>
            View list <ArrowRightAltIcon className="icon" />
          </p>
        </div>
      </Card>
    </Grid>
  );
};

export default memo(Cardbox);
