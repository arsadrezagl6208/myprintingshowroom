import { memo } from "react";
import { Menu } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { useAppDispatch } from "../../../../app/hooks";
import dashboardIcon from "../../../../assets/assets/navsider/dashboard_icon.svg";
import importDataIcon from "../../../../assets/assets/navsider/import_data_icon.svg";
import dataMappingIcon from "../../../../assets/assets/navsider/data_mapping_icon.svg";
import jobsIcon from "../../../../assets/assets/navsider/jobs_icon.svg";
import settingIcon from "../../../../assets/assets/navsider/setting_icon.svg";
import logIcon from "../../../../assets/assets/navsider/log_icon.svg";
import logout from "../../../../assets/assets/navsider/log_icon.svg";
import "./NavSider.scss";
import { nameFind } from "../../../../tokenDetails";
import { RoleFind } from "../../../../tokenDetails";
import jwt_decode from 'jwt-decode';

const NavSider = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate()


  const logoutfromform =()=>{
      localStorage.removeItem("token");
      localStorage.removeItem("role")
      navigate("/login");
  }

//   const nameFind =()=>{
//     const token=localStorage.getItem("token");
//     const decoded = jwt_decode<any>(token!= null?token:"");
//     if(decoded!=null){
//       console.log(decoded.jti);
//       return decoded.jti
//     }
//     else {
//       return "Undefined Name"
//     }
// }
// const RoleFind =()=>{
//   const token=localStorage.getItem("token");
//   const decoded = jwt_decode<any>(token!= null?token:"");
//   if(decoded!=null){
//     console.log(decoded.iss);
//     return decoded.iss
//   }
//   else {
//     return "Undefined Name"
//   }
// }
  return (
    <div className="navSider-section">
      <div className="user">
        <img
          src="https://qph.cf2.quoracdn.net/main-thumb-749105445-200-exweizutvfcedtxyrggdumcqugttvywn.jpeg"
          alt="Loading..."
        />
        <div onClick={() => navigate('/main/profile')}>
          <p className="userName"> { nameFind()}</p>
          <p className="userDest" >{RoleFind()}</p>
        </div>
      </div>
      <Menu
        theme="light"
        className="menu"
        mode="vertical"
        defaultOpenKeys={["1"]}
        defaultSelectedKeys={["1"]}
        style={{ borderInlineEnd: "none" }}
      >
        <Menu.ItemGroup className="menuItems" style={{ marginBottom: "50px" }}>
          <Menu.Item key="1">
            <Link to="Dashboard" className="listItems">
              <img src={dashboardIcon} className="icon" />
              DashBoard
            </Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to="profile" className="listItems">
              <img src={importDataIcon} className="icon" /> Profile
            </Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link to="updateProfile" className="listItems">
              <img src={dataMappingIcon} className="icon" /> Edit
            </Link>
          </Menu.Item>
          <Menu.Item key="4">
            <Link to="changePass" className="listItems">
              <img src={jobsIcon} className="icon" /> Change Password
            </Link>
          </Menu.Item>
          <Menu.Item key="5">
            <Link to="settings" className="listItems">
              <img src={settingIcon} className="icon" /> My Settings
            </Link>
          </Menu.Item>
          {'ROLE_ADMIN'===RoleFind() && <Menu.Item key="6">
            <Link to="auditLogs" className="listItems">
              <img src={logIcon} className="icon" /> Admin
            </Link>
          </Menu.Item> }
           
        </Menu.ItemGroup>
        <div className="menuItems">
          <Menu.Item className="menuItem">
            <div
              className="listItems"
              // onClick={() => dispatch(setUser("logout"))}
            >
              <div className="logout">
                <img src={logout} className="icon" />
              </div>
              <div className="logoutText">
                <p onClick={() => logoutfromform()}>Log Out</p>
                {/* <p>logout from panel</p> */}
              </div>
            </div>
          </Menu.Item>
        </div>
      </Menu>
      <div className="navimg">
        <p>Reconciliation Data</p>
        <div className="img" />
      </div>
    </div>
  );
};

export default memo(NavSider);
