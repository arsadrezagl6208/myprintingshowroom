import React, { Suspense } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { UpdateDetails } from "../../EditProfile/EditProfile";
import MainPage from "../../mainPage";
import ProfilePage, { UpdatePassword } from "../../profile/Profile";
import Dasboard from "./Dashboard1";

// const Dashboard = React.lazy(() => import("../pages/dashboard/Dashboard"));
// const ImportData = React.lazy(() => import("../pages/importdata/ImportData"));
// const Profile = React.lazy(() => import("../pages/profile/Profile"));

const AppFlow = () => {
    const base_dataScience_url = "/services/data-science";
    return (
        <Suspense>
            <Routes>
                <Route
                    path=""
                    element={<Navigate to='/main/Dashboard' replace />}
                />
                <Route
                    path="Dashboard"
                    element={<Dasboard />}
                />
                <Route
                    path="profile"
                    element={<ProfilePage />}
                />
                <Route
                    path="changePass"
                    element={<UpdatePassword />}
                />
                <Route
                    path="updateProfile"
                    element={<UpdateDetails />}
                />
                <Route
                    path="profile"
                    element={<ProfilePage />}
                />
                <Route
                    path="/*"
                    element={<Navigate to='/404' replace />}
                />
            </Routes>
        </Suspense>
    );
};

export default AppFlow;
