import { memo } from "react";
import { Layout, Menu } from "antd";
import { Link } from "react-router-dom";
import Sider from "antd/es/layout/Sider";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import AssignmentOutlinedIcon from "@mui/icons-material/AssignmentOutlined";
import HomeRepairServiceOutlinedIcon from "@mui/icons-material/HomeRepairServiceOutlined";
import AutoAwesomeMosaicOutlinedIcon from "@mui/icons-material/AutoAwesomeMosaicOutlined";
import AccessTimeFilledOutlinedIcon from "@mui/icons-material/AccessTimeFilledOutlined";
import PowerSettingsNewIcon from "@mui/icons-material/PowerSettingsNew";
import DashboardCustomizeIcon from "@mui/icons-material/DashboardCustomize";

const NavLayout = () => {
  return (
    <Layout>
      {/* <Layout> */}
      <Sider
        breakpoint="lg"
        // collapsedWidth="0"
        onBreakpoint={(broken) => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
        theme="light"
      >
        <Menu
          theme="light"
          //   style={{ background: "none" }}
          mode="vertical"
          defaultSelectedKeys={["1"]}
        >
          <Menu.Item key="1">
            {/* <Link to="/counter"> */}
            <DashboardCustomizeIcon fontSize="medium" /> DashBoard
            {/* </Link> */}
          </Menu.Item>
          <Menu.Item key="2">
            <Link to="/mnj">
              <AccessTimeFilledOutlinedIcon fontSize="medium" /> Import Data
            </Link>
          </Menu.Item>
          <Menu.Item key="3">
            <AutoAwesomeMosaicOutlinedIcon fontSize="medium" /> Data Mapping
          </Menu.Item>
          <Menu.Item key="4">
            <HomeRepairServiceOutlinedIcon fontSize="medium" /> Managing Jobs
          </Menu.Item>
          <Menu.Item key="5">
            <SettingsOutlinedIcon fontSize="medium" /> My Settings
          </Menu.Item>
          <Menu.Item key="6">
            <AssignmentOutlinedIcon fontSize="medium" /> Audit Logs
          </Menu.Item>
        </Menu>
      </Sider>
      <Sider theme="light">
        <Menu mode="vertical" theme="light">
          <Menu.Item>
            <PowerSettingsNewIcon fontSize="medium" /> Log Out
          </Menu.Item>
        </Menu>
      </Sider>
      {/* </Layout> */}
      {/* <Layout style={{background:'inherit'}}> */}
      {/* <Header className="header"> */}
      {/* <div className="logo" />
        <Menu
          // theme="dark"
          mode="horizontal"
          style={{ background: "none" }}
          // defaultSelectedKeys={["2"]}
        >
          <Menu.Item>Nav 2</Menu.Item>
          <Menu.Item>Nav 3</Menu.Item>
        </Menu>

        <Content style={{ margin: "24px 16px 0" }}> */}
      {/* <div
            style={{
              padding: 24,
              minHeight: 599,
              background: colorBgContainer,
            }}
          >
            <RouterFlow /> 
          </div> 
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2023 Created by Ant UED
        </Footer> 
      </Layout> */}
    </Layout>
  );
};

export default memo(NavLayout);
