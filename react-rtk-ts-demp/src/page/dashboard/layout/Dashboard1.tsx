import { Grid } from "@mui/material";
import "./Dashboard.scss";
import Card from "./card/Card";
const Dasboard = () => {
  return (
    <div className="dashboard">
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <Card
            value={950}
            contentHeading1={"Total"}
            contenHeading2={"Reconcile"}
            color={"#4C68EF"}
          />
        </Grid>
        <Grid item xs={4}>
        <Card
            value={890}
            contentHeading1={"Accepted"}
            contenHeading2={"Reconcile Data"}
            color={"#50DFB2"}
          />
        </Grid>
        <Grid item xs={4}>
          <Card
            value={60}
            contentHeading1={"Rejected"}
            contenHeading2={"Reconcile Data"}
            color={"#FC9644"}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default Dasboard;
