import { memo } from "react";
import { Input } from "antd";
import Grid from "@mui/material/Grid/Grid";
import TuneOutlinedIcon from "@mui/icons-material/TuneOutlined";
import ContainerWrapper from "../../../../utils/wrapper-tag/ContainerWrapper";
import img from "../../../../assets/assets/navheader/logo.png";
import NotificationSvg from "../../../../assets/assets/navheader/notification.svg";
import MessageSvg from "../../../../assets/assets/navheader/message.svg";
import SearchIconSvg from "../../../../assets/assets/navheader/searchIcon.svg";
import "./NavHeader.scss";
import { nameFind } from "../../../../tokenDetails";


const NavHeader = () => {
  return (
    <ContainerWrapper className="navHeader-main">
      <div className="navHeader">
        <Grid container spacing={3}>
          <Grid item xs={2}>
            <p style={{ fontWeight: 600, fontSize: "25px" }}>Arsad Technology</p>
            {/* <div className="logo">
              <img src={img} alt="" />
            </div> */}
          </Grid>
          <Grid item xs={6}>
            <div className="title">
              <h3 className="heading">DASHBOARD</h3>
              <p className="subheading">Hello {nameFind()}, Welcome Back!</p>
            </div>
          </Grid>
          <Grid item xs={3}>
            <Input
              placeholder="Search here..."
              type="text"
              prefix={
                <img
                  src={SearchIconSvg}
                  alt="SearchIcon"
                  className="searchIcon"
                />
              }
              suffix={<TuneOutlinedIcon />}
              bordered={false}
            />
          </Grid>
          <Grid item xs={1}>
            <div className="navbar">
              <img src={MessageSvg} alt="Message" className="icon" />
              <img src={NotificationSvg} alt="Notification" className="icon" />
            </div>
          </Grid>
        </Grid>
      </div>
    </ContainerWrapper>
  );
};

export default memo(NavHeader);
