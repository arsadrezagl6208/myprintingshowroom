import { Grid } from '@mui/material'
import { Button, Form, Input, } from 'antd'
import { useNavigate } from 'react-router-dom';
import { CityFind, CompanyFind, EmailFind, nameFind, RoleFind } from '../../tokenDetails';
import './Profile.scss'
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from 'react';
import { editData } from '../../actions/edit.action';
import { getEmailById } from '../../app/fetchApiByEmail/userActions';
import { RootState } from '../../app/fetchApiByEmail/store';

export interface DataType {
    email: string | '';
    name: string | '';
    companyName: string | '';
    city: string | '';
    role: string | '';
}


export const UpdateDetails = () => {
    const [city, setCity] = useState<string>();
    const [fname, setName] = useState<string>();
    const [companyName, setCompanyName] = useState<string>();
    const [email, setEmail] = useState<string>();
    const [role, setRole] = useState<string>();
    const [users, setUsers] = useState<DataType>();
    const nav = useNavigate();
    const [form] = Form.useForm();
    const dispatch = useDispatch();


    const onFinish = (formData: DataType) => {

        // if (formData.email === undefined ) {
        //     formData.role = RoleFind();
        //     formData.email = EmailFind();
        // }
        // if (formData.city === undefined) {
        //     formData.city = CityFind();
        // }
        // if (formData.companyName === undefined) {
        //     formData.companyName = CompanyFind();
        // }
        // if (formData.name === undefined) {
        //     formData.name = nameFind();
        // }

        console.log('props change', formData)
        dispatch<any>(editData(formData));
        // localStorage.removeItem("token");
        // nav("/login")
        form.resetFields();
    };

    useEffect(() => {
        dispatch<any>(getEmailById(EmailFind())).then((res:any)=>{
            setCity(res.payload.city);
            setName(res.payload.name);
            setCompanyName(res.payload.companyName);
            setEmail(res.payload.email)
            setRole(res.payload.role)
            setUsers(res.payload)
            console.log('name', res.payload.name ,fname,city,companyName,email,'details ',users,res.payload);
      });
      console.log("Dubbger call" );
      dispatch<any>(editData(users));
    },[]);
    
    return (
        <div>
            <h3 className='headng'>Update your accounts {fname} </h3>
            <Form
                layout="vertical"
                form={form}
                initialValues={{remember:true}}
                onFinish={onFinish}
            >
                <Grid container rowSpacing={1} columnSpacing={{ xs: 1 }}>
                    <Grid item xs={5}>
                        <Form.Item label="Name" name="fname" initialValue={fname}>
                            <Input placeholder=" Name" />
                        </Form.Item>
                    </Grid><br />
                    <Grid item xs={5} >
                        <Form.Item label="Email"  initialValue={email}>
                            <Input placeholder="email" disabled />
                        </Form.Item>
                    </Grid><br />
                    <Grid item xs={5} >
                        <Form.Item label="City" name="city">
                            <Input placeholder="City"  />
                        </Form.Item>
                    </Grid><br />
                    <Grid item xs={5}>
                        <Form.Item label="Company" name="companyName">
                            <Input placeholder="Company"  />
                        </Form.Item>
                    </Grid><br />
                    <Grid item xs={5}>
                        <Form.Item label="Role" name="role" >
                            <Input placeholder="Role" disabled />
                        </Form.Item>
                    </Grid><br />
                    <br></br>
                    <Grid item xs={8} className='form-control-btns'>
                        <Button type="primary" htmlType="submit">Submit</Button>
                        {/* <Button  onClick={abc} style={{marginLeft:10}}>Cancel</Button> */}
                        {/* <Button style={{ marginLeft: 10 }}>Cancel</Button> */}
                    </Grid>
                </Grid>
            </Form>
        </div>
    );
}



