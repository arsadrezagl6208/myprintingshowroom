import { useState } from 'react';
import { Grid } from '@mui/material'
import { Button, Form, Input, Menu, MenuProps } from 'antd'

import './Profile.scss'
import { nameFind } from '../../tokenDetails';
import { UpdateDetails } from '../EditProfile/EditProfile';
import { useNavigate } from 'react-router-dom';

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
        type,
    } as MenuItem;
}



const navigationMenu = [
    { label: 'Account Details', value: 'acc' },
    { label: 'Password Update', value: 'pass' },
    // { label: 'License & Access details', value: 'lad' },
]

const items: MenuProps['items'] = navigationMenu.map(item => getItem(item.label, item.value))

const ProfilePage = () => {
    const navigate = useNavigate()
    const [menuItem, setMenuItem] = useState<typeof navigationMenu[0]["value"]>("acc")
    const onClick: MenuProps['onClick'] = (e) => {
        console.log('click ', e);
        setMenuItem(e.key)
    };

    const editProfile=()=>{
        navigate("/main/updateProfile")
    }

    return (
        <div className='outer-cont'>
            <div className='template'>
                <h3>Profile Settings</h3>
            </div>
            <div className='outer-div' >
                <Grid container className='inner-div'>
                    <Grid item xs={3}>
                        <div className='profile-update'>
                            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBASDBAQEhAKCRAKDQwICQoKCREJCggMJSEZJyUhJCQcLi4lKSwrHyQWJjg0Ky8xNTU1GjFIQDs0PzA0NTEBDAwMEA8QGBESGjQhGB0xND8xMTQ/NDQ0MTQ0NDQxNDExMTQxNDQ0MT8xMT8/MTQ/MTE/PzE/NDExNDQxMTExNP/AABEIAMgAyAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEDBAUGBwj/xABBEAACAQIDBQYDBgIIBwEAAAABAgADEQQSIQUiMUFRBhMyYXGBkaGxFCNCUsHRB3IkM2KCkqLh8BUXQ1NUY/E0/8QAGgEAAgMBAQAAAAAAAAAAAAAAAgQBAwUABv/EACgRAAICAgEEAgEEAwAAAAAAAAABAhEDITEEEkFREyJxBTJCUhVhof/aAAwDAQACEQMRAD8A8vIuIa2trrDCawu5Y8NfeOAKL5RCQPSCt/OTGiw42XyMZaZI5C2v80nR1O+CO/7QlG7zhBdddPSGL/6yHomMWyIg9Y99OclZB6+/igkD1v8A5YKpkuLQC1nHB3X+VyslXFVRwd/c5vrAy6cx7RjpxtJ7YvwRbS5JPtjnj3b2/Nh6bfpCXHsD4Ka6fgD0/oZAB7RBZzxx9EqUl5NlKzMgJ7xg66kYh/1vK5opfXMPIqjfpNLCp/R6Z43ReUCtR3tOn+aKqSTaRqwwqai35RQTDgOh+7IDXI0X6SttKmTXdwEIqPnHd+BfITbo0VNI51Dkm5JEo4nCJm0BSx5GTjyJyv0V5+lcY0uDFKnmCPaNaXaqWa3ThIbRtUzLlBp0RZYgJLljFPScR2tAfOOD7x7RhIaI2hwT6eQj8owFuse8hx0En7CJ3eZ84oLsLR4FMnuRpjZwPOmL/wDtyyansV73GUeffD952mJ7IOr5TTYDxitRcPSYcuOoPtaXdm9lEA0Vi3AmoA2WIz65JWnZp/Hj5OAbZznSxJAsQ4y5W9dbSpWwj8lZddAKeXNPWa3ZJStjmbnooVWmLi+xjHNZium6oUt+sDH+oRupOiJY4SWmecJQZWsysOgZcuaWkoC3C99dPCs6Z+yFdiCCCCealWWaGE7HMPEyg8dAVyrL59fiSuwYYUnvg4sYe/4SPK/ijHC6eEa9DvTuK3ZB/wALrfmGPi6cpaw3ZIhSCpJ5DvA1P95S/wBRxJadsueKFHmeJDI2QWGl7gh5VI66zp+2exmwr0b2y1FqW3QmVgRf148Zzdpp4JxyQUlwzOzRSk0uBhGIhAR7S2itI3MDVfuUFhZV3f7UfEVGOmW2Y8RJ9npfD0z/AGdD+XWFiaJuB1OhmbKUe9qjewpdsa9GbidoqrZBmbKVDH8OaSGstRM4vYm2oyt7yxiNioxzHOpbKTrlVmgth0ppkXQIdLnNm85bF43XbyUSllcpKXHgyMQNZDaT1+PvIwI1GOrM2atgKIiISjWJxOaBS0RkQSIZEa2s5IBoKA4kpEhcmdTOlpAhd6NCpjUxSaK9ejutnfxNxiKVr08LtG5zCo6Gi6jpu6W9pcr/AMUavdMKWFwuHdhZapZ6ip7HjPObR4T6HF/VErNLhs7T/mftTrgmv1wg3fnF/wAzNpcxgT5nC/6zi4oEv0/C+YolZZLyeg4P+KFdUAq4XCYlgWLulRsOW16C4Es1P4rNcZMDSAA3xUxLMxbysJ5rFBl+nYH/ABC+aXs9KT+K2uuz6XnlxR/aWKf8VqV97AVT1yYofqJ5aTFKf8Xgu6J+aR2HbXtbT2ktAJh3whwrVHZqlUVO8UgaCw8pykGmNZLbWOYsMcUVGPCOUnLbIwNY8IjWK0s7Tqo63ZFO+EpcPC31hY6na3M/5s0m2Il8FS05Nb4mSYuhe3A2MwZussvya+CVJfgzT3gTx8eGYZpnYl3PiIPpN2pS3ToPeY+KWNYKbtIHNJ1yYz3Le8YgyW291hZZoeDPqysQY9iZJUHTrJMvtIfsHtd0VSCIIOsncSMCclYLWxFoBMdhr7wXElRBk2CDHjqNPWKdQFM6nEdi8Wo0ps/lbLOfxuAqUWy1EekRycZZ74nbDCnoNbW7xTMrbXajBFCxwuHxrqciCqtOoA3vCj1M7pxsXjS82eHfCNaeh1Nu4Ek95sXBNfxGkVTj6GRnaexCTn2LUS3E0sU183oDLfkf9GXLaPP7RwJ6V2g2Ps59gPjsNgn2fUo1adNQtcu6i4F2BJFiD68J5vaTCamm6qtEVsG0QhRQ+0kOkN72khGsCj4vaTsPrBcdlseCPJHAh/vCKTu0PR2nZ5HOCpeHKFa1xvLqZJjqFXPocq7trDxQ+zQP2Gn6Nb4malU7rcrK3GeezaySdeRzHNqjEqoch5TExye06SogyneX0JmBtJhYnQ8tJd0zbdE5JaMO29/pCKwFfeJ68oRcTTcWLJoBxvD1hEQHqXYcQBCLi0hxo5NbInEjAhu8EOISWiuTSYJ4xEaRi2sdn0nJbAtUxAaRogYpJ2jY+0NZrWO9f/8AJmmjsvCtXZiUp1ArqBT+zCmcQxHXy5mZtOldiNDnK2/pWXLed1s7CBEQIMjIjBKiuHz1Drrrwk9Zklih9FcmB0OKE5/Z0kctt3Y9TDVArJSCVU7yky4XvLW4gkcxMw0hrdaSXRXu+GqL8xPVsW9KrhTRrPTJqIoeolRKeVuoN73nndTCla2QHvAqsiVKe0CytY6RfoOqyZYNZItNf9LepjGDuLVM7XZGE77s7tCjZbNQ7ykqjLlYLfnrxAnkQU5b/Ez2nsfVvi3w7Cy18G9RyXDZmuotb0Jnk2JwLpVrUgrE0KtSk35VsSP0jXTfumn+RdO0mZ+TT6RWlt8O6oCQBmMr5Y52p8HUPRG8JYYacpBSG8OdzLtSlu+ggSjTLY8MoUzv+plsj5SrSF3HLXSWqw0t1nONtIFSpNl+j2jr06CUaS0k7vNeoy53e5vwOgjp2rxYBzGlVBFrNSC5fS0y6OBqO26juDmIZUOXTjJamzqqsylKjd342Vc2XS+spl02Bt9yVgfPJPTOs2Nj6dfDuAgRwVp1Eb7zdI4gyjjsE7kopKCmc9h+WYeycS9LEIw0F+7YE7rKdNZ3ODoZmq217tVB0y8ZnZcXwTbjw+BpZnKD9nDbQwpprfq1tZRo6nW+k6btZSK0U0OtS17eU5vDjj62juJuUE2URf2SZIy6+sZlt8NTJCOsZhpIYxqiBx85FWQA6XPrJX0kNQ3MKMSjI1VeRIIRH1jJHPGQ+SI8BWjTT7P0FqbRwqMj4lKmIpCrRpjM1SnfUeluPleKVSydrqi1LRvUdjOzWD0lOTJ/Vlt3rLo2C44um7/6jvfOehD7CvDCVR/cP7wjicLywrnrcD94Ms8peDPgq5kjz/8A4CwVbNRJpsz27nxX5cZHS2PiAEsuEVkza5Du66WnohxmG/8AEB9csIY7Df8AiqP8EH5p1VFmv7HNdncPl2tRql6aFUqYWoBT3q11015ai9pV29senTx+LJOf7XV+05O7y5bgGwPredauPw4YMMLTBUh1bMLqRwPCY+2cR31YOUCEqFsDmZbczAUpd18aOlJRjp2zjcTsZCoQ1Gpd4+SkvdZmZrXsPhxmG2wa2a16C6tYmr4vlO32ogD4XiL4hdfy7rc5w2JoP3r7x8bHxluZj/TylJckRm3yDU2Q9MZ2ak2VrZUbMYTpdT5i0FKBDC5J14Ey2SOYyG/IbsvkmmrdjEJJox0Wzc9DNQbKerhu+RhuVlwwphd5r87yKpQvw9RabOwcYAn2cpUc98uKDpvIq2sbjl5GDlk1G1yiucqTSNPs9Uw9Jkw7ugZDYmsmRajX5XnXjZ1FN9UpjOGvZhl1mbVwtM0iwFN2qC2Q0w7MxlirspKqojhj3aroKr0/XgZkZZ27tqyvGrdtWcRt7ZtJNo0lpqR3mbEVB4l0PKDtTF4hGbuahwwc2qaZWa3DlOsfZtOlwXMcv2dahJdkubkC/tMXauHABPTgTCjmUmovdDkYrtd6s47F1K9QgVHqVsuqZicqecrpRKtY63F+E2Xp3J5ayvUTeXnZl1mgpJKkqBjFJ2UCp6EwXBPlNN6f0v8Ayypk4+uhg3ZcigUvGNIeUsuNZGRrJVlcoqyEp6CDbekxEiI1kENJcGn2fdRjabMQirmLMwLLlseQI+sUo4Z1D3bdAGmmaKUyx27s6/8AZ7GaPm/+KVsRSrZtxyBzBjJSfk5NjzbLJ6dCqSAGzXNrZw2aQo9u2zKUoz0lRTNDEW/rCNNTcQ6WFrllvUdyQoyhgqs15uYvZbiiWAY92v3ua1qmmpEzKeamy2y6DOtz4WEiM1JOqDePsaUrFjaLLVQi6IwZLA7rMDrJKlO7f3bSxUpmpVoIScrvbQeFja5ljtDhBQRGTM5dihzNw0g6tLyyWtOXhHO7XQXw9hbJiFN7+JbGYFfZaFmN3OZmPHw6zXrYl2ZFIW2dTf8AEspYnFVAzWy6FhfLGIRlGktAQyRt2zOfZijUZ9NZcwXZ/EYjLlQIjf8AVqXVGXyHEzS7MUnr4z7yzpSRqxQru1GvYfrPRqdMBdABpwE7JmlB09sajJSX1OK2X2Hp02JrO2KuN1EBoonzuZrVti0aWFqrRpU6BqBTUZBmepY8yZ0BX56Rso1B1voRFpZZS22RKLerPNldDVfPUxNPu3VDRpNqyjoOk2sMarVVWnU+0pURqoRl+8or1J6esr7X2NSauxfvKJUaPSfu+8XkZs9kNnLTo1HAqffsqU2qnM9SmBofIEkyJ9so35Ax90ZUZ1Si6sc6stjcFvC3pMfaq7p53no1SkGGUgMCNQ05HtVs1adMVFuFdsjqfwN5SiGOpJpjvyWqZwZQ7wPNtJVrpb4r9ZpVhxsC3I23ZSrBirHIRusBcjdmjF2QnoirCyt9ZWA3efGTVnOVeBzDlKzu/kvtJimWxeiGp4pERJWHz1kZWWeDttgNI1Gp9ZKw/wDkBBp7wWgXyDkJ4Zf70UIPbkG11uYoNkUe2PhByB+Mu7KwqrVzvYCnqoJy5mm2Nn+ghjAjy+ETnlUouN8iOPA4yUmuAftKXILIyZdCAWbNzmHjMIhe6eHey7vhnQjBrH+xrF8dQbaY1OLmqaOXxGGOVDruBSPzSvtNGdUuxNs3jOadXXwQy6chwmFtLD7o5axrHNNoVy43FM5utQtbVOK6CnvN7ytiaJ45j+K+UDdmlVDeE9VteA6W6Dp/ajXc0Jxim9EHZl8mOUa/eoyG5zeY+k9AThOBTD5a9KqhFNqbq7j8Lrz+RnfUTcfpFs+9jmBNWggNfSM409I7fDqIztp18vzShDL4KGOwSVHS4zBMxqL+Fl5A+8u5Mq6actPwwgLDzY3Mao+lvK5k23oGkrYxP+sxu1qKdnVL2BBVqd/xNeadA5gGF98XGYeFZi9t1/oIOpy1kv8AOEl9kjk7R5+U042lashKn0Yyyz7vW/QyJz6mNK7CVpGSx3Rw3MyfOV3P+zLNSwqOL20Vxf8AF1lao69QdZekWxeiI8fSRsfeSZhrIXPvJoJMZzBRTlHnzMjdif2kvLrpAegeXogI1jxfGKCTR9T5hGzCAUBb1F7R8ijkB6zK0RsfvF6wgbxgB5fCFaRolWCw0PpMbamGITNpoes2jwmTtGqShBta/C0sx3eirNXa7OVxiaHjw0lU3U2a5IyuBNLEj6yDG0rkdci2ImgnwmZdU2/RANWXnd1sPeddhTdVPlYzk8At6wB0yb4nT4RiBb3BlWSOqGMUk2mXn4fQyNdT84d9ORJErvcHS/vF0vAxKVInapY/TWUcXicqm3HhHYHU8bC7MzZEUdSekzMRWR0V0dMQr656Z00NrCS2oqwY3N1wjdw50v1WYnbgH/hz2sWNSllF/ObOGe6gjduFsDOZ7e1mFKilzZ3d3tu5rDT6wo7kg48UjgXLqNLaaE3lZy5Gr2Hluy5Wa+nDTSUnG7yAA1jkfwXxjoo1Fs4N82YNInELEtZh5HlGqPf/AH4Zd6OrYCDQ/X8siIhF+PmeUjd7dJGyeEQuNfeSHh7SENc8uNzDd9PpIkDFq2wCfQRoJHrFBo7uPqhvEPhJDBY8PIwTU8mPtMemFaCU+RHrCMjznkp9zCUnmAJx1oc8Ji487p56zaPCYmPO43rLcXJTnWjCrHX1MjqvfoLC2kKsB6+sqliG/MOv5Y+lZkyk1a9h06y02DkFggsbccpmjhtvYbTf7ssbAVFK/PhMDaD2sLkXGtpkvbgADzliwqatlTzuDpHqKtpcEEEXBG9mhFQR9Z59s7tFVoqqbuIRfCrnKyeQM6jAbZ71M4o1ksbbwCq3oYpPDKHI7h6lZNU7L+LwFOshp1FNRG40yxUN8DIcFsKhRQoiEo7d7lqVC6q3QeUs4TFZ2yhSpUXBY+L0lqtVyLmYqoUaktlVZS2/2jKSW+AEp5eHwnBfxBx6F6CqWc01q52CnIrEgAeuk6VO1OCqO9GnWR6qs1Jaag/ePzseBlPaWzqdakyMmdagtpusnmDyMJNwknJFkGnweX1sTp09TKVbFed78hLXaTYNXBtclqtCo1qVXwsrdD5zEtfnNOCjKKkmWub4SJK1a46yNqxPlBYesGWNIDudjlj1IkTmSDjAbjK/J0roZIYN4KDSENOsGTJitIR4RQS0UAl0fVdorCZL7R6H4SF9oHqfjMxYpMB54o2yw6ge8BqyjmJgPjiTx4yGtimHG4vqDyaGsPtgvPq0tG9XxqhTa55XlXE0lOGZ7knLmFzpeYbYw5babuYgne4wftjdwVuxHIX3VhrE1Veyp51K79EFSVnP/wAkpe8hcxqJnzae0Zu1G3ltzH+GZbn18wZf2id7rYKJmv8A7H5o5jX1Qhkf2Z0Ow6FNMK1Y0BjaoZjTpnxKvAceHWdDhSxpZ6iilZc7jPuo3Meg6zg8BtetSZadPu37x9M6lst5u7QxddqWUEMD/WKBvVk5iJZscu9t+TW6bLBQXhkLdrQtV+4U1GBalRdv6rLzYjifITMx20K+IuKtV6ik27sHJT+A0mfWVKbXQAd6N1AMvd+UelUGbW736RvFigkpJbEOqzZHJq9ES4XJWRqbLTqK6vTc+FWHI+U9BwmM7ykpIyPupVS+bI3l1E4nDjNWHLKc/wAJs4bEOrEgkq6qMv5WBinWx7mq5Q10GWSX2ei72lwaV8DVRrKcjVqbfkqLrPHQ2k9A7UbbxFNSi0iKeIptTGJNT/qHiQBwsOR6zz8i3tpC6SMoxd8M1FJS2hEwbxMY6D6RmUqJVt0L/ekiYyVzpITBvydLWian4Y5MYDSRmr5fOVPbD7lFJMNooAe/7RSNkdyPcDivXXzj0nd2CqC5PBV4mZDYnhotjz8WWXcBtE0a4cDMAN5fzg+cqek65MeEra7no2Ew7I16gFPS6hj4oGMxdNqeUlVK60wu9l+Eo7U2x37KSuRUDAKhzMfUzMLdLiVxg5NOTpoafUxjFwirT8l01BlNjfSOj/df3pSB068jOiw2Lwy7OIPdd4EdSpTM5qG9v0lkpUlSvYvBKTduqRkI8ROnWVBV3tOR4ScPfXj5Q+7wVSXkyMXVvUbyPOUnPLqOMGvWvUbndmgO9unSPRVJCEk3Iu7Gw18RnOoprcfzHhN55R2JTtRLf9xrj+UaTSA0imWTcx3HGoo5PHozYipqqKraKN2DTKrew4aE/mg49r131/G2o9ZAXtGoqoqhSacm9mngwAzMeYUJ0Xy9ZZOMQcLt6CNs1FqYcXAOrAyjiqLU21vbrfwxCU1Kbi+UaUcTjjjKJdqVRUQo6h0qcaZG6vn6zlNt7G7ol6d6lIt4vE9Fuh/ebyYjhwOXkR4lk1Kr4gVV0qaOp3lbyMOMnB64Dx5WmefOmsEG06XbexQgarRBZBrUojeaj6eU5htYyu2atDkcqq0C738ohGIjgQJJrRMW5OyYnTnwleTOd0yCVWFl8IUUUU6ys9Oapbi3LlAesd0j8Yvp+WPFBXgy1ww6bueGYycOy8bxRQZckRF9pFuvpBbEbj8RqtoopHgsj5IBWytxveXExACMxIACsSScuWNFD8oj+LObasLk8desheqxYKBmZiqKB+YxRTR8MojFWdnhENOgiHUoqhrb29zlpDcdLamKKZsv3F3g4ys/3r/2nY/ORuYopoLhCr5NfZSvToJUN3SoWLC2tHXQ+k1npLUTk4I0tFFMbqdTbXNm1024KzAx2FekxtmKnQEfhkVKp6mw/wAUUUYxu47KckUpOi5TNhxvzOswe0Gxw169Bb3361FB4fMD6xRSccmpaCgcvaGg1iijj4GYcj1hp7yCKKLyJyciMUUUgA//2Q==" alt='profile' />
                            <h3>{nameFind()}</h3>
                            <div className='btn-group'>
                                <Button onClick={editProfile}>Update</Button>
                                {/* <Button>Remove</Button> */}
                            </div>
                        </div>
                        <Menu
                            onClick={onClick}
                            defaultOpenKeys={['acc']}
                            mode="inline"
                            items={items}
                            className='user-setting-nav-menu'
                        />
                    </Grid>
                    <Grid item xs={9} className='right-grid'>
                        {/* <h3 className='heading'>Account Details</h3> */}
                        {menuItem === "acc" && <AccountDetails />}
                        {menuItem === "pass" && <UpdatePassword />}
                        {/* {menuItem === "lad" && <p>lad</p>} */}
                    </Grid>
                </Grid>
            </div>
        </div>
    )
}


export const AccountDetails = () => {

    const [form] = Form.useForm();

    const onFormLayoutChange = (props: any) => {
        console.log('props change', props)
    };

    return (
        <div>
            <h3 className='heading'>Account Details</h3>
            <Form
                layout="vertical"
                form={form}
                initialValues={{ layout: "vertical" }}
                onValuesChange={onFormLayoutChange}
            >
                <Grid container rowSpacing={1} columnSpacing={{ xs: 1 }}>
                    <Grid item xs={12} md={6}>
                        <Form.Item label="First Name">
                            <Input placeholder="First Name" />
                        </Form.Item>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Form.Item label="Last Name">
                            <Input placeholder="Last Name" />
                        </Form.Item>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Form.Item label="Email">
                            <Input type='email' placeholder="Email" />
                        </Form.Item>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Form.Item label="Phone">
                            <Input placeholder="Phone" />
                        </Form.Item>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Form.Item label="Company">
                            <Input placeholder="Company" />
                        </Form.Item>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Form.Item label="Designation">
                            <Input placeholder="Designation" />
                        </Form.Item>
                    </Grid>
                    <Grid item xs={12}>
                        <Form.Item label="Bio">
                            <Input.TextArea placeholder="Bio" />
                        </Form.Item>
                    </Grid>
                    <Grid item xs={12} className='form-control-btns'>
                        <Button type="primary">Submit</Button>
                        <Button>Cancel</Button>
                    </Grid>
                </Grid>
            </Form>
        </div>
    );
}


export const UpdatePassword = () => {

    const [form] = Form.useForm();

    const onFinish = (LoginDetails: any) => {
        console.log("LoginDetail ", LoginDetails)
        form.resetFields();
    }
    function ResetFields() {
        form.resetFields();
    }
    const validateConfirmPassword = (_: any, value: any) => {
        if (value && value !== form.getFieldValue('password')) {
            return Promise.reject(new Error('Password and confirm password not match. '));
        } else {
            return Promise.resolve();
        }
    };
    return (
        <div>
            <h3>Password Update</h3><br />
            <Form
                layout="vertical"
                form={form}
                autoComplete="off"
                initialValues={{ layout: "vertical" }}
                onFinish={onFinish}
                scrollToFirstError
            >
                <Grid item xs={12} md={6}>
                    <Form.Item
                        name='old-password'
                        label="Current Password" rules={[{
                            required: true,
                            message: 'Please input your password!'
                        }]}>
                        <Input type='password' placeholder="Enter your current password" required />
                    </Form.Item>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Form.Item name='password' label="New Password" rules={[
                        {
                            required: true,
                            message: 'Please input your password!'
                        }, {
                            min: 8,
                            message: 'Password must be at least 8 characters!',
                        }, {
                            pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/,
                            message: 'Password should contain at least 1 lowercase 1 uppercase 1 digit and 1 special character',
                        }]}>
                        <Input type='password' placeholder="Enter new password" required />
                    </Form.Item>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Form.Item name='confirm-password' label="Confirm Password"
                        dependencies={['password']}
                        rules={[
                            {
                                required: true,
                                message: 'Please input your confirm password!',
                            },
                            { validator: validateConfirmPassword },
                        ]}

                    >
                        <Input type='password' placeholder="Enter new confirm password" required />
                    </Form.Item>
                </Grid>
                <br />
                <Grid item xs={12} className='form-control-btns'>
                    <Button type="primary" htmlType="submit">Update Password</Button>
                    <Button onClick={ResetFields} style={{marginLeft:10}}>Cancel</Button>
                </Grid>

            </Form>
        </div>
    );
}


export default ProfilePage