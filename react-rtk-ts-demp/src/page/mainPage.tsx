import { Button, Form, Input } from "antd";
import 'react-toastify/dist/ReactToastify.css';
import Grid from '@mui/material/Grid';
import ContainerWrapper from "../Register-Login/ContainerWrapper";


const MainPage = () => {
 
  return (
    <>
      <ContainerWrapper className="form-section-main-container ">
        <Grid container className="section">
          <Grid item xs={7} className="element-wrapper">
            <div>
              <h2>Hello Arsad</h2>
            </div>
          </Grid>
          <Grid item xs={5}>
            <div className="form-wrapper">
              <Grid item>
                <Form
                  
                  autoComplete="off"
                  
                  style={{ maxWidth: 600 }}
                  scrollToFirstError
                >

                  <Form.Item
                    className="box-size"
                    name="username"
                    label="E-mail"
                    rules={[
                      {
                        type: 'email',
                        message: 'Email is not valid!'

                      },
                      {
                        required: true,
                        message: 'Please input your E-mail!',
                      },
                    ]}
                  ><Input placeholder="Enter your Email" className="align-box" />
                  </Form.Item>
                  <Form.Item
                    className="box-size"
                    name="password"
                    label="Password"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your password!',
                      }, {
                        min: 6,
                        message: 'Password must be at least 6 characters!',
                      }, {
                        pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/,
                        message: 'Password length is less tahn 6 ',
                      }
                    ]}
                    hasFeedback
                  >
                    <Input.Password placeholder="Enter your password" className="align-box" />
                  </Form.Item>
                
                  {/* <Form.Item>
                    <ReCAPTCHA
                      sitekey="6LcDDK4kAAAAAPZ-ra9ziNFupvfBLTxu3fmXUWsj"
                      onChange={onCaptchaChange}
                    />
                  </Form.Item> */}
                  <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit" >
                      Submit
                    </Button>
                    <Button type="primary"  style={{ marginLeft: 10 }}>
                      Register
                    </Button>
                  </Form.Item>
                </Form>
              </Grid>
            </div>
          </Grid>
        </Grid>
      </ContainerWrapper>
    </>
  );
};

export default MainPage;
