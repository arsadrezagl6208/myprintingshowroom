import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { dataReducer } from "../a_reducers/register.reducer";
import { configureStore } from '@reduxjs/toolkit';
import { authSlice } from "../slices/authSlice"; 

export const store1 = createStore(
    combineReducers({
      data: dataReducer,
    }),
    applyMiddleware(thunk)
  );



const store = configureStore({
  reducer: {
    auth: authSlice.reducer,
  },
  // middleware: (getDefaultMiddleware) =>
  //   getDefaultMiddleware().concat(loginUser),
});

export type RootState1 = ReturnType<typeof store.getState>;
export default store;

  

