import { configureStore } from '@reduxjs/toolkit';
import cakeReducer from '../features/cake/cakeSlice'
import iceCreamReducer from '../features/icecream/icecreamSlice'
import userReducer from '../features/user/userSlice'
//CakeReducer
// const cakeReducer = require('../features/cake/cakeSlice')
//IceCreamReducer
// const iceCreamReducer = require('../features/icecream/icecreamSlice')
//UserReducers
// const userReducer = require('../features/user/userSlice')
//Import End
const store = configureStore({
    reducer:{
        cake: cakeReducer,
        icecream: iceCreamReducer,
        user:userReducer,
        
    }
})


export default store 
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch