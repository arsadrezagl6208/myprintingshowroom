import { configureStore } from '@reduxjs/toolkit';
import { getByEmailidSlice } from './createSliceStore';

const store = configureStore({
  reducer: {
    users: getByEmailidSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;