import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";
interface UserData1 {
    name:string,
    companyName:string
    role:string
    city:string
}
export const getEmailById = createAsyncThunk<UserData1, string>(
    'users/getByEmailid',
    async (email: string) => {
      const response = await axios.get<UserData1>(`http://localhost:8281/findBy/${email}`,{
        method: "GET",
            headers: {
              "Content-Type": "application/json",
              "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
      });
      console.log(response.data,"Arsad reza Data By Arsad")
      return response.data;
    }
  );