

import { createSlice, } from '@reduxjs/toolkit';
import { getEmailById } from './userActions';
export interface UserData1 {
    name:string,
    companyName:string
    role:string
    city:string
}

interface GetByEmailidState {
  data: UserData1 | null;
  isLoading: boolean;
  error: string | null;
}

const initialState: GetByEmailidState = {
  data: null,
  isLoading: false,
  error: null,
};


export const getByEmailidSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getEmailById.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getEmailById.fulfilled, (state, action) => {
        state.isLoading = false;
        state.data = action.payload;
        state.error = null;
      })
      .addCase(getEmailById.rejected, (state, action) => {
        state.isLoading = false;
        state.data = null;
        state.error = action.error.message ?? 'Unknown error';
      });
  },
});