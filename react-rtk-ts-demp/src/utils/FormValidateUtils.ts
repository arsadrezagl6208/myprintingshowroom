export const phoneNumber = (value: string) => {
  return value && !/^[0+][0-9]{9,15}$/.test(value)
    ? "Invalid phone number"
    : undefined;
};

export const requiredField = {
  message: "This field is required",
};

export const emailPattern = {
  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,8}$/i,
  message: "Please enter a valid email Id",
};

export const urlPattern = {
  value: /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g,
  message: "Please enter a valid web url",
};

export const phonePattern = {
  value: /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/i,
  message: "Please enter a valid mobile number",
};

export const passwordPattern = {
  value: /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
  message: "Password should include at least one uppercase, one numeric value and one special character",
};

export const otpPattern = {
  value: /^[0-9]{6,6}$/,
  message: "Password should be of 6 digits",
};