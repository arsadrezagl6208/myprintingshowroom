import axios from "axios";
import config from "./config";
import jwtDecode from 'jwt-decode';

type Props = {
  headers?: any;
  anonymous?: any;
};
const http = (headers?: any, anonymous?: any) => {
  const defaultHeaders = {};

  return axios.create({
    baseURL: config.BASE_API_URLS_REGISTER,
    timeout: config.TIMEOUT,
    headers: {
      ...defaultHeaders,
      ...headers,
    },
  });
};
//@ts-ignore
http().interceptors.response.use(
  (res:any)=>{
    console.log('abcabcabcabacabav ',res);
    return res;
  }
);
export default http;
interface LoginData {
  username: String;
  password: String;
}

export const loginapi = async (data: LoginData) => {
  
  const response = await http().post('http://localhost:8281/token', data);
  
    localStorage.setItem("token",response.data.token)    
  return response;

} 