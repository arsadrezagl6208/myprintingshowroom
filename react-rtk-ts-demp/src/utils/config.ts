
const config = {
  BASE_API_URLS_REGISTER: "http://localhost:8281/save",
  TIMEOUT: 900000, // 15 minutes = 60x15x1000 = 900000
};

export default config;

