import { Navigate } from "react-router";

export function isAuthenticated() {
    // Check if the user is authenticated, for example by checking if there is a token in local storage
    const token = localStorage.getItem('token');
    return !!token;
  }

  export const ProtectedRoute1 = ({ children }: { children: React.ReactElement }) => {
    //const user = useAppSelector(getUser);
    const token = localStorage.getItem('token');
    if (token === "" || !token)
        return <Navigate to="/login" replace />
    else
        return children
}