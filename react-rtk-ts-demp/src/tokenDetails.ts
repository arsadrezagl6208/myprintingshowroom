import jwt_decode from 'jwt-decode';
export const nameFind =()=>{
    const token=localStorage.getItem("token");
    const decoded = jwt_decode<any>(token!= null?token:"");
    if(decoded!=null){
      console.log(decoded.jti);
      return decoded.jti
    }
    else {
      return "Undefined Name"
    }
}
export const RoleFind =()=>{
  const token=localStorage.getItem("token");
  const decoded = jwt_decode<any>(token!= null?token:"");
  if(decoded!=null){
    console.log(decoded);
    return decoded.iss
  }
  else {
    return "Undefined Role"
  }
}
export const EmailFind =()=>{
  const token=localStorage.getItem("token");
  const decoded = jwt_decode<any>(token!= null?token:"");
  if(decoded!=null){
    console.log(decoded.sub);
    return decoded.sub
  }
  else {
    return "Undefined Email"
  }
}
export const CompanyFind =()=>{
  const token=localStorage.getItem("token");
  const decoded = jwt_decode<any>(token!= null?token:"");
  if(decoded!=null){
    console.log(decoded.company);
    return decoded.company
  }
  else {
    return "Undefined Company"
  }
}
export const CityFind =()=>{
  const token=localStorage.getItem("token");
  const decoded = jwt_decode<any>(token!= null?token:"");
  if(decoded!=null){
    console.log(decoded.city);
    return decoded.city
  }
  else {
    return "Undefined City"
  }
}