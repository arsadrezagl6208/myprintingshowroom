export const lightTheme = {
    bodyBg: "#F9F9F9",
    headings: "black",
}
export const darkTheme = {
    bodyBg: "#141a2c",
    headings: "white",
    text: "#c1c1c1",
}