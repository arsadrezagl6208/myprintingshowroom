import { Button, Form, Input } from "antd";
import ContainerWrapper from "./ContainerWrapper";
// import { FormInstance } from 'antd/lib/form';
// import ReCAPTCHA from 'react-google-recaptcha';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Grid from '@mui/material/Grid';
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { loginUser } from "./users/userSlice";

export interface DataType {
  username: String;
  password: String;
}


const Login = () => {
  const nav = useNavigate();
  const [formReset] = Form.useForm();
  const dispatch = useDispatch();

  const onFinish = (Student: DataType) => {
    const username = Student.username;
    const password = Student.password
    console.log('arsad ', Student.username, ' ', Student.password)
   dispatch<any>(loginUser({username, password })).then((res:any)=>{
    if(res.payload === 200){
      toast("sdsff")
      nav("/main/Dashboard")
    } 
})
  toast("arsadsarsaddsff")

    // if(statu==='200')
    // {
    //   
    // }
    formReset.resetFields();
  };
  
  const register = () => {
    nav("/")
  }
  return (
    <>
      <ContainerWrapper className="form-section-main-container ">
        <Grid container className="section">
          <Grid item xs={7} className="element-wrapper">
            <div>
              <h2>Hello Arsad</h2>
            </div>
          </Grid>
          <Grid item xs={5}>
            <div className="form-wrapper">
              <Grid item>
                <Form
                  form={formReset}
                  autoComplete="off"
                  onFinish={onFinish}
                  style={{ maxWidth: 600 }}
                  scrollToFirstError
                >

                  <Form.Item
                    className="box-size"
                    name="username"
                    label="E-mail"
                    rules={[
                      {
                        type: 'email',
                        message: 'Email is not valid!'

                      },
                      {
                        required: true,
                        message: 'Please input your E-mail!',
                      },
                    ]}
                  ><Input placeholder="Enter your Email" className="align-box" />
                  </Form.Item>
                  <Form.Item
                    className="box-size"
                    name="password"
                    label="Password"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your password!',
                      }, {
                        min: 6,
                        message: 'Password must be at least 6 characters!',
                      }, {
                        pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/,
                        message: 'Password length is less tahn 6 ',
                      }
                    ]}
                    hasFeedback
                  >
                    <Input.Password placeholder="Enter your password" className="align-box" />
                  </Form.Item>
                
                  {/* <Form.Item>
                    <ReCAPTCHA
                      sitekey="6LcDDK4kAAAAAPZ-ra9ziNFupvfBLTxu3fmXUWsj"
                      onChange={onCaptchaChange}
                    />
                  </Form.Item> */}
                  <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit" >
                      Submit
                    </Button>
                    <Button type="primary" onClick={register} style={{ marginLeft: 10 }}>
                      Register
                    </Button>
                  </Form.Item>
                </Form>
              </Grid>
            </div>
          </Grid>
        </Grid>
      </ContainerWrapper>
    </>
  );
};

export default Login;
