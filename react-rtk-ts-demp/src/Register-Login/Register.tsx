import { Button, Form, Input, Select } from "antd";
import ContainerWrapper from "./ContainerWrapper";
import Grid from '@mui/material/Grid';
import { useNavigate } from "react-router-dom";
import { postData } from "../actions/register.action";
import { useDispatch } from "react-redux";

export interface DataType {
  email: String;
  password: String;
  name: String;
  companyName: String;
  city: String;
  role: String;
}


const Register = () => {
  const nav = useNavigate();
  const dispatch = useDispatch();
  const [formReset] = Form.useForm();

  const onFinish = (formData: DataType) => {
    const role = formData.role.toUpperCase();
    if(role==='ADMIN' || role.toUpperCase()==='ADMIN' || role.includes("ADMIN")){
      formData.role='ROLE_ADMIN';
    }
    if(role==='NORMAL' || role.toUpperCase()==='NORMAL' || role.includes("NORMAL")){
      formData.role='ROLE_NORMAL';
    }
    console.log('arsad ', formData.email, ' ', formData.password, formData.role)
    dispatch<any>(postData(formData));
    
    formReset.resetFields();
    
  };

  
  const login = () => {
    nav("/login")
  }


  return (
    <>
      <ContainerWrapper className="form-section-main-container ">
        <Grid container className="section">
          <Grid item xs={7} className="element-wrapper">
            <div>
              {/* <h2 className="color-heading">Get</h2>
              <h3 className="heading-text">Reconciled</h3>
              <h3 className="heading-text">Data with TRS</h3> */}
              <h2>Hello Arsad</h2>
            </div>
          </Grid>
          <Grid item xs={5}>
            <div className="form-wrapper">
              <Grid item>
                <Form
                  form={formReset}
                  autoComplete="off"
                  onFinish={onFinish}
                  style={{ maxWidth: 900 }}
                  scrollToFirstError
                >

                  <Form.Item
                    className="box-size"
                    name="email"
                    label="E-mail"
                    rules={[
                      {
                        type: 'email',
                        message: 'Email is not valid!'

                      },
                      {
                        required: true,
                        message: 'Please input your E-mail!',
                      },
                    ]}
                  ><Input placeholder="Enter your Email" className="align-box" />
                  </Form.Item>
                  <Form.Item
                    className="box-size"
                    name="password"
                    label="Password"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your password!',
                      }, {
                        min: 6,
                        message: 'Password must be at least 6 characters!',
                      }, {
                        pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/,
                        message: 'Password length is less tahn 6 ',
                      }
                    ]}
                    hasFeedback
                  >
                    <Input.Password placeholder="Enter your password" className="align-box" />
                  </Form.Item>
                  <Form.Item
                    className="box-size"
                    name="name"
                    label="Name"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your Name!',
                      },

                    ]}
                  ><Input placeholder="Enter your Name" className="align-box" />
                  </Form.Item>
                  {/* <Form.Item
                    className="box-size"
                    name="role"
                    label="Role"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your Role!',
                      },

                    ]}
                  ><Input placeholder="Enter your Role" className="align-box" />
                  </Form.Item> */}
                  <Form.Item
                    className="box-size"
                    name="city"
                    label="City"
                    rules={[

                      {
                        required: true,
                        message: 'Please input your City!',
                      },
                    ]}
                  ><Input placeholder="Enter your City" className="align-box" />
                  </Form.Item>
                  <Form.Item
                    className="box-size"
                    name="companyName"
                    label="Company Name"
                    rules={[

                      {
                        required: true,
                        message: 'Please input your Company Name',
                      },
                    ]}
                  ><Input placeholder="Enter your Company Name" className="align-box" />
                  </Form.Item>
                  <Form.Item
                  className="box-size"
                  name="role"
                  label="Role "
                  rules={[

                    {
                      required: true,
                      message: 'Please select your Role',
                    },
                  ]}
                  >
                  <Select showSearch
                    placeholder="Select a person"
                    optionFilterProp="children" 
                    filterOption={(input, option) =>
                      (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                    }
                    options={[
                      {
                        value: 'ROLE_ADMIN',
                        label: 'ROLE_ADMIN',
                      },
                      {
                        value: 'ROLE_NORMAL',
                        label: 'ROLE_NORMAL',
                      },
                      {
                        value: 'ROLE_ARSAD',
                        label: 'ROLE_ARSAD',
                      },
                    ]}
                    />
                    </Form.Item>
                  <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit" >
                      Register
                    </Button>
                    <Button type="primary" onClick={login} style={{ marginLeft: 10 }}>
                      Login
                    </Button>
                  </Form.Item>
                </Form>
              </Grid>
            </div>
          </Grid>
        </Grid>
      </ContainerWrapper>
    </>
  );
};

export default Register;
