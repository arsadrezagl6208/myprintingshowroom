import { POST_DATA_FAILURE, POST_DATA_REQUEST, POST_DATA_SUCCESS } from "../actionTypes/actionTypes";

// Define initial state
interface State {
    data: any[];
    loading: boolean;
    error: any;
  }
  
  const initialState: State = {
    data: [],
    loading: false,
    error: null,
  };
  
  // Create reducer
  export const dataReducer = (state = initialState, action: any) => {
    switch (action.type) {
      case POST_DATA_REQUEST:
        return {
          ...state,
          loading: true,
        };
      case POST_DATA_SUCCESS:
        return {
          ...state,
          data: action.payload,
          loading: false,
          error: null,
        };
      case POST_DATA_FAILURE:
        return {
          ...state,
          data: [],
          loading: false,
          error: action.payload,
        };
      default:
        return state;
    }
  };
  