import { EDIT_DATA_FAILURE, EDIT_DATA_REQUEST, EDIT_DATA_SUCCESS } from "../actionTypes/actionTypes";

// Define initial state
interface State {
    data: any[];
    loading: boolean;
    error: any;
  }
  
  const initialState: State = {
    data: [],
    loading: false,
    error: null,
  };
  
  // Create reducer
  export const dataReducer = (state = initialState, action: any) => {
    switch (action.type) {
      case EDIT_DATA_REQUEST:
        return {
          ...state,
          loading: true,
        };
      case EDIT_DATA_SUCCESS:
        return {
          ...state,
          data: action.payload,
          loading: false,
          error: null,
        };
      case EDIT_DATA_FAILURE:
        return {
          ...state,
          data: [],
          loading: false,
          error: action.payload,
        };
      default:
        return state;
    }
  };
  