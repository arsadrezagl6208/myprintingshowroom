import {
  POST_DATA_FAILURE,
  POST_DATA_REQUEST,
  POST_DATA_SUCCESS,
} from "../actionTypes/actionTypes";
import { EmailFind } from "../tokenDetails";

// Define action creators
export const postDataRequest = () => ({
  type: POST_DATA_REQUEST,
});

export const postDataSuccess = (data: any) => ({
  type: POST_DATA_SUCCESS,
  payload: data,
});

export const postDataFailure = (error: any) => ({
  type: POST_DATA_FAILURE,
  payload: error,
});


// Define async action to send data to API
export const postData = (formData: any) => {
  return async (dispatch: any) => {
    dispatch(postDataRequest());
    try {
      const response = await fetch(`http://localhost:8281/save`, {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
          
        },
      });
      const data = await response.json();
      dispatch(postDataSuccess(data));
    } catch (error) {
      dispatch(postDataFailure(error));
    }
  };
};


