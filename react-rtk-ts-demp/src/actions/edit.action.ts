import {
    EDIT_DATA_FAILURE,
    EDIT_DATA_REQUEST,
    EDIT_DATA_SUCCESS,
  } from "../actionTypes/actionTypes";
  import { EmailFind } from "../tokenDetails";
export const editDataRequest = () => ({
    type: EDIT_DATA_REQUEST,
  });
  
  export const editDataSuccess = (data: any) => ({
    type: EDIT_DATA_SUCCESS,
    payload: data,
  });
  
  export const editDataFailure = (error: any) => ({
    type: EDIT_DATA_FAILURE,
    payload: error,
  });
export const editData = (formData: any) => {
    return async (dispatch: any) => {
      dispatch(editDataRequest());
      try {
        console.log("Edit data Abdullah !!",`${localStorage.getItem("token")}`,"tokrn",formData)
        const response = await fetch(
          `http://localhost:8281/edit/${EmailFind()}`,
          {
            method: "PUT",
            body: JSON.stringify(formData),
            headers: {
              "Content-Type": "application/json",
              "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
          }
        );
        const data = await response.json();
        dispatch(editDataSuccess(data));
      } catch (error) {
        dispatch(editDataFailure(error));
      }
    };
  };