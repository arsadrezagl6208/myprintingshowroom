import React from "react";
import { Result, Button } from "antd";

export default class ErrorBoundary extends React.Component<any> {
  state = {
    hasError: false,
    error: {},
    info: {},
  };

  processError = () => {
    this.setState({
      hasError: false,
      error: {},
      info: {},
    });
  };

  componentDidCatch(error = {}, info = {}) {
    this.setState({
      hasError: true,
      error,
      info,
    });
  }

  render() {
    if (this.state.hasError) {
      return (
        <Result
          status="500"
          title="500"
          subTitle="Sorry, something went wrong."
          extra={<Button type="primary">Back Home</Button>}
        />
      );
    }

    return this.props.children;
  }
}