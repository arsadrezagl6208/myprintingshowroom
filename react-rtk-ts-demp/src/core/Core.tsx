import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ThemeProvider} from "styled-components";
// import { ThemeProvider } from "@emotion/react";
import {GlobalStyle} from "../web/theme/GlobalStyle";
import { darkTheme, lightTheme } from "../web/theme/Theme";
import ErrorBoundary from "./ErrorBoundary";
import Register from "../Register-Login/Register";
import Login from "../Register-Login/LoginPage";
import Page404 from "../Page404";
import Abc from "../Register-Login/Abc";
import RouterFlow from "./RouterFlow";

function Core() {
  return (
    <ThemeProvider theme={lightTheme}>
      <BrowserRouter>
        <ErrorBoundary>
        <RouterFlow />
        {/* <Routes>
              <Route path="/" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/404" element={<Page404 />} />
            </Routes> */}
      
        </ErrorBoundary>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default Core;
