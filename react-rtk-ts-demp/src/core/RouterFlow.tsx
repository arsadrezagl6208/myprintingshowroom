import React, { Suspense } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { Dashboard } from "../page/dashboard/Dashboard_create";
import Register from "../Register-Login/Register";
import { ProtectedRoute1 } from "../utils/Utils";

const Login = React.lazy(() => import("../Register-Login/LoginPage"));
const Page404 = React.lazy(() => import("../Page404"));

const RouterFlow = () => {
  const base_dataScience_url = "/services/data-science";

  return (
    <Suspense>
      <Routes>

        {/* <Route path="/" element={<Abc />} /> */}
        <Route path="/" element={<Register />} />
        <Route path="/login" element={<Login />} />
        {/* <Route path="/mainpage" element={<MainPage />} /> */}
        <Route path="" element={<Navigate to="/main" replace />} />
        <Route
          path="main/*"
          element={
            <ProtectedRoute1>
              <Dashboard />
            </ProtectedRoute1>
          }
        />
        <Route path="/404" element={<Page404 />} />

      </Routes>
    </Suspense>
  );
};

export default RouterFlow;
